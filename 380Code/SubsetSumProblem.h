#ifndef SUBSETSUMPROBLEM_H
#define SUBSETSUMPROBLEM_H

/**
 * @version 3
 * @author Dr Kamal Bentahar, based on Java code by Dr Mike Morgan
 */

#include "Base.h"
#include "rand/mt64.h"

class SSP
{
protected:
    u64 S[ MAX_SET_SIZE ];
    int n;
    u64 t;

public:
    /* Constructors and initializers */
    SSP() // Constructor - Create an empty SSP instance (n=0) with target t=0
    {
        t=0;
        n=0;
        init_genrand64(time(0)); // Use current time as seed for the random numbers generator
    }
    void init(u64 integers[], int size, u64 target) // Initialize SSP instance's paramaters
    {
        t = target;
        n = size;
        for(int i=0; i<size; i++)
            S[i]=integers[i];
    }

    /**
    * getDataFromUser(): Asks the user to input S and t for the SSP instance
    * (does not remove duplicate elements)
    */
    void getDataFromUser()
    {
        cout << "Please enter the size of the problem set: n=";
        cin >> n;

        cout << "S = {x1, x2, ... , x" << n << "}" << endl;
        cout << "Please enter the values:" << endl;
        for(int i=0; i<n; ++i)
        {
            cout << "\tx" << (i+1) << " = ";
            cin >> S[i];
        }
        cout << "\tEnter the target value: t=";
        cin >> t;
        cout << endl;
    }

    /* Random SSP instances
     * If guaranteed=true it generates an SSP instance that is guranteed to be solvable
     */
    void randominstance(int instanceSize, int bitlength, bool guaranteed)
    {
        u64 MASK = (1ull<<bitlength)-1ull;
        n = instanceSize;
        t = 0;
        for(int i=0; i<n; i++)
        {
            S[i] = random64() & MASK;
            if( rand() > RAND_MAX/2 )
                t += S[i];
        }
        if( !guaranteed )
            t = random64();
    }

    void printssp() // Prints the problem instance, listing the elements of S and the values of t and n
    {
        cout << "SSP:";
        cout << "\n\tS = {";
        if (n!=0) cout << S[0];
        for(int i=1; i<n; ++i)
            cout << ", " << S[i];
        cout << "}";
        cout << "\n\tn = " << n << "\n\tt = "  << t << endl;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bool sortandtrim(); // return 'true' if instance has been trimmed; 'false' otherwise
};

#endif // SUBSETSUMPROBLEM_H
