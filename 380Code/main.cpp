#include "Base.h"
#include "SubsetSumProblem.h"
#include "SolutionContainer.h"
#include <stdio.h>

/*
play around with n (and numsSize) to see the effect, try small n, 5 then increase to 10 and to 100 and see the effect
Currently we gave you a randomiser that tries, randomly different solution and if you are lucky enough (depends on n) it will find the solution
try to come up with a better way of deciding the SSP problem

As a side note: when you change the number of elements inside the set make sure to change the buffer of your console otherwise you might not be able to see the original set
*/
int main()
{
   /*
    u64 distance, i=0;

    //sspInstance.getDataFromUser();
    int n = 10;
    int numsSize = 7;
    sspInstance.randominstance(n, numsSize, true); // n=5, elements of S are < 2^7=128, and the SSP instance is guaranteed to be solvable
    //sspInstance.sortandtrim();*/
    
    int n = 10;
    int numsSize = 7;
    
    //define how many sets you want on following line
    #define Subsets 3
    SolutionContainer sspInstance[Subsets];
    //UNCOMMENT THE FOLLOWING 3 LINES BELOW TO GENERATE 3 RANDOM SAMPLING INSTANCES
    sspInstance[0].randominstance(n, numsSize, true);
    sspInstance[1].randominstance(n, numsSize, true);
    sspInstance[2].randominstance(n, numsSize, true);
    
    //PREDETERMINED SETS (Uncomment to use these sets and target values)
   // sspInstance[0].init(new (u64[10]){3,4,5,8,10,50,100, 150, 170, 188}, 10,84);
   // sspInstance[1].init(new (u64[15]){1,2,3,10,20,30,100,200,250,265,346,454,546,626,736}, 15, 262);
    
    for(int i=0; i<Subsets; i++)
    {
        //Uncomment following line to use the form to enter your own values and target value, done using sortandtrim()
      //  sspInstance[i].getDataFromUser();
        sspInstance[i].printssp();
        //If statement to sort the set and then trim anything about target value 
        if( sspInstance[i].sortandtrim() == true )
        {
            cout << "\nSSP instance sorted and trimmed:" << endl;
            sspInstance[i].printssp();
            
        }
        
        //Each method has its own clock to time execution and if statement to state if true or false
        
        clock_t start1 = clock();
        //cout << "\nExhaustive: " << boolalpha << sspInstance[i].exhaustivesearch() << endl;
        if (sspInstance[i].exhaustivesearch() == true)
            printf("\nExhaustive: Found a subset with given sum (TRUE)");
        else
            printf("\nExhaustive: No subset with given sum (FALSE)");
        std::cout << "\nElapsed time for Exhaustive: " << (std::clock() - start1) / (double)CLOCKS_PER_SEC << " ms" << std::endl;
        
        clock_t start2 = clock();
        //cout << "\nGreedy: " << boolalpha << sspInstance[i].greedyAlgorithm() << endl;
        if (sspInstance[i].greedyAlgorithm() == true)
            //  sspInstance[i].printsol();
            printf("Greedy: Found a subset with given sum (TRUE)");
        else
            printf("\nGreedy: No subset with given sum (FALSE)");
        std::cout << "\nElapsed time for Greedy: " << (std::clock() - start2) / (double)CLOCKS_PER_SEC << " ms" << std::endl;
        
        clock_t start3 = clock();
        //cout << "\nTrivial: " << boolalpha << sspInstance[i].istrivial() << endl;
        if (sspInstance[i].istrivial() == true)
            printf("\nTrivial: Found a subset with given sum (TRUE)");
        else
            printf("\nTrivial: No subset with given sum (FALSE)");
        std::cout << "\nElapsed time for Trivial: " << (std::clock() - start3) / (double)CLOCKS_PER_SEC << " ms" << std::endl;
        
        clock_t start4 = clock();
        //cout << "\nGRASP: " << boolalpha << sspInstance[i].heuristic(1,501) << endl;
        if (sspInstance[i].GRASPAlgorithm(1,500) == true)
            printf("\nGRASP: \nFound a subset with given sum (TRUE)");
        else
            printf("\nGRASP: \nNo subset with given sum (FALSE)");
        std::cout << "\nElapsed time for GRASP: " << (std::clock() - start4) / (double)CLOCKS_PER_SEC << " ms" << std::endl;
        
        clock_t start5 = clock();
        //cout << "\nBFS: " << boolalpha << sspInstance[i].bfs() << endl;
        if (sspInstance[i].bfs() == true)
            printf("\nBFS(Dynamic): Found a subset with given sum (TRUE)");
        else
            printf("\nBFS: No subset with given sum (FALSE)");
        std::cout << "\nElapsed time for BFS: " << (std::clock() - start5) / (double)CLOCKS_PER_SEC << " ms" << std::endl;
        
        cout << "----------------------------------------------" << endl;
    }
    
    
    
    
    
  /*
    for(int i=0; i<Subsets; i++)
    {
        sspInstance[i].printssp();
        if( sspInstance[i].sortandtrim() == true )
        {
            cout << "\nSSP instance trimmed:" << endl;
            sspInstance[i].printssp();
        }
        clock_t begin = clock();
        
        cout << "\nGreedy: " << boolalpha << sspInstance[i].greedyAlgorithm() << endl;
        
        clock_t end = clock();
        double elapsed_secs =double (end -begin) /CLOCKS_PER_SEC;
        //cout<<endl<<"Elapsed seconds for Greedy: " << elapsed_secs<<endl;
        
        std::cout << "Elapsed seconds for Greedy: " << (std::clock() - begin) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
    }
    */
    
    
   
    
 /*   do // Try random subsets until a solution is found
    {
       cout << "Exhaustive: " << boolalpha << endl;
        cout << "Greedy: " << boolalpha << endl;
        cout << "Trivial: " << boolalpha << endl;
        cout << "Grasp: " << boolalpha << endl;
        cout << "BFS: " << boolalpha << endl;

        cout << "\nAttempt #" << ++i << ":" << endl;
        distance = sspInstance.tryRandomElements();
        sspInstance.printsol();
    } while( distance != 0 );*/

    return 0;
}
