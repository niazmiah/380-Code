#ifndef SOLUTIONCONTAINER_H
#define SOLUTIONCONTAINER_H

/**
 * @version 3
 * @author Dr Kamal Bentahar, based on Java code by Dr Mike Morgan
 */

#include "Base.h"
#include "SubsetSumProblem.h"

class SolutionContainer : public SSP
{
    bool decision;                  // 'true' of SSP is satisfied; 'false' otherwise
    bool isElement[ MAX_SET_SIZE ]; // isElement[i] is true if S[i] is included in the subset, otherwise it is false
    int  numEls;                    // Number of elements in the current subset "solution"
    u64  total;                     // Total sum of the elements in the current subset "solution"

public:

    SolutionContainer() // Constructor
    {
        reset();
    };
    void reset()
    {
        numEls=0;
        total=0;
        for(int i=0; i<MAX_SET_SIZE; i++) isElement[i]=false;
    }

    inline u64 distanceFromTarget() // Returns the absolute distance between the subset sum and the target value
    {
        return ( t>total ? t-total : total-t );
    }

    void printsol()
    {
        bool comma=false;
        cout << "SSPsolve:" << endl;
        cout << "\tdecision = " << boolalpha << decision << endl;
        cout << "\tsubset   = {";
        for(int i=0; i<n; ++i)
            if(isElement[i])
            {
                if(comma)
                    cout << ", " << S[i];
                else
                {
                    cout << S[i];
                    comma=true;
                }
            }
        cout <<
             "}\n\tnumEls   = " << numEls <<
             "\n\ttotal    = " << setw(20) << total <<
             " (t=" << t << ")" <<
             "\n\tdistance = " << setw(20) << distanceFromTarget() <<
             "\t= " << 100*(double)distanceFromTarget()/t << "% of t" << endl;
    }

    /**
    * Adds an element to the subset
    * @param   i   The index of the element to be added
    * @return      The absolute difference between the subset sum and the target value, after the element has been added
    */
    u64 addElement(int i)
    {
        if( isElement[i]==false )
        {
            isElement[i]=true;
            total += S[i];
            numEls++;
        }
        return distanceFromTarget();
    }
    /**
    * Removes an element from the subset
    * @param   i   The index of the element to be removed
    * @return      The absolute difference between the subset sum and the target value, after the element has been removed
    */
    u64 removeElement(int i)
    {
        if( isElement[i]==true )
        {
            isElement[i]=false;
            total -= S[i];
            numEls--;
        }
        return distanceFromTarget();
    }

    u64 tryRandomElements()
    {
        reset(); // Reset solution container
        for(int i=0; i<(rand()%n); i++) // Include a random number of elements
            addElement(rand()%n);       // Add a random element
        decision = (total == t);
        return distanceFromTarget();
    }

    u64 swap(int i, int j)
    {
        //cout << isElement[i] << "~" << isElement[j] << endl;
        if(isElement[i]==isElement[j])
            return total;
        if(isElement[i])
        {
            total -= S[i];
            total += S[j];
        }
        else
        {
            total += S[i];
            total -= S[j];
        }
        isElement[i]=!isElement[i];
        isElement[j]=!isElement[j];
        return distanceFromTarget();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bool solve();
    bool istrivial();
    bool istrivialAlgorithm(int size, u64 sum);
    bool exhaustivesearch(int, u64);
    bool exhaustivesearch() { return exhaustivesearch(n, t); } // shortcut
    bool bfs();
    bool greedyAlgorithm();
    bool GRASPAlgorithm(int size, u64 sum);
    bool heuristicSpecialCases(int size, u64 sum);
    bool isSubsetSum(int size, u64 sum);
    
    u64 greedy(vector<u64> &selected);
    u64 GRASP(int ans[], int size, u64 sum, int i, int total, int distance);
};

#endif // SOLUTIONCONTAINER_H
