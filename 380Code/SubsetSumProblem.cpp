#include "SubsetSumProblem.h"
#include <iomanip>

bool SSP::sortandtrim() // return 'true' if instance has been trimmed; 'false' otherwise
{
    sort(S,S+n);
    
    for(int i=0; i<n; i++)
    {
        if( S[i] > t )
        {
            n=i;
            return true;
        }
    }
    
    return false;
}