#include "Base.h"

u64 random64(void)
{
    return genrand64_int64();
}

u64 random64(u64 a, u64 b)
{
    return (a+(random64()%(b-a+1)));
}

/**
 *  Recursive implementation of the binary GCD algorithm **/
u64 gcd(u64 a, u64 b) // c.f. http://en.wikipedia.org/wiki/Binary_GCD_algorithm
{
    // Simple cases (termination)
    if (a == b) return a;
    if (a == 0) return b;
    if (b == 0) return a;
    // Look for factors of 2
    if ( ~a&1 ) // a is even
    {
        if ( b&1 ) return gcd(a/2, b); // b is odd
        else return 2*gcd(a/2, b/2);   // both a and b are even
    }
    if ( ~b&1 ) return gcd(a, b/2); // a is odd, b is even
    // Reduce larger argument
    if ( a>b )  return gcd((a-b)/2, b);
    return             gcd((b-a)/2, a);
}


