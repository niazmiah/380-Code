#ifndef BASE_H_INCLUDED
#define BASE_H_INCLUDED

// New type: "unsigned 64 bit integers"
#define u64 unsigned long long  //or: uint64_t

#define MAX_SET_SIZE   100  // Maximum size of the set S
#define MAX_BIT_LENGTH  64  // Maximum bit length of the integers in S

#include <iostream>         // for: cout, etc.
#include <iomanip>          // for: setw()
#include <ctime>            // for: time()
#include <algorithm>        // for: sort()
#include "rand/mt64.h"      // for: 64 bit pseudo-random numbers generation
#include <vector>

using namespace std;

u64 random64();             // random 64 bit number
u64 random64(u64 a, u64 b); // random numbers between a and b
u64 gcd(u64 a, u64 b);      // Greatest Common Divisor of a and b

#endif // BASE_H_INCLUDED
