#include "SolutionContainer.h"

//Trivial
bool SolutionContainer::istrivialAlgorithm(int pos, u64 target)
{
  //  std::clock_t    start;
  //  start = std::clock();
    
    int totalsum = 0;
    for(int i=0; i<7; i++)
    {
        totalsum = totalsum + S[i];
    }
    int smallestelementins = S[0];
    for(int i=0; i<7; i++)
    {
        if(S[i] < smallestelementins) smallestelementins = S[i];
    }
    
   // std::cout << "\nTime: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
    if (target > totalsum) return true;
    if (target < smallestelementins) return false;
    return false;
    
}
bool SolutionContainer::istrivial()
{
    return istrivialAlgorithm(n, t);
}

//Exhaustive
bool SolutionContainer::exhaustivesearch(int pos, u64 sum)
{
  /*  std::clock_t    start;
    start = std::clock();
    std::cout << "Time: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;*/
    
    if (sum == 0)       return true;
    else if (pos == 0)  return false;

    if (S[pos-1] > sum)
        return exhaustivesearch(pos-1, sum);
    
     return exhaustivesearch(pos-1, sum) || exhaustivesearch(pos-1, sum-S[pos-1]);
    
    
    return false;
}

//bfs(Dynamic)
bool SolutionContainer::bfs()
{
    return isSubsetSum(n, t);
}

bool SolutionContainer::isSubsetSum(int pos, u64 sum) {
    bool subset[sum+1][pos+1];
    for(int i = 0; i <= pos; i++) subset[0][i] = true;
    for (int i = 1; i <= sum; i++) subset[i][0] = false;
    for (int i = 1; i <= sum; i++) {
        for (int j = 1; j <= pos; j++) {
            subset[i][j] = subset[i][j-1]; if (i >= S[j-1])
                subset[i][j] = subset[i][j] || subset[i - S[j-1]][j-1]; }
    }
    return subset[sum][pos];
}

//Greedy
u64 SolutionContainer::greedy(vector<u64> &selected)
{
    
    int total = 0;
    
    for (int i = 15; i >= 0; i--)
    {
        if (S[i] <= (t - total)) {
            total = total + S[i];
            selected.insert(selected.end(), S[i]);
            
        }
    }
    //printsol();
   return (t-total);
  //  return distanceFromTarget();
}

bool SolutionContainer::greedyAlgorithm()
{
    vector<u64> selected;
    
    u64 greedyresult = greedy(selected);
    vector<u64>::iterator it;
    
    cout << "\nGreedy: \nGreedy subset:";
    for (it=selected.begin(); it<selected.end(); it++) {
        cout << ' ' << *it;
    } cout << '\n';
    
    
    
    cout << "Distance from target: " << greedyresult << endl;
    
    if (greedyresult == 0)
        return true;
    
    
    
return false;
}

//Grasp
int position[MAX_SET_SIZE]; int position2[MAX_SET_SIZE];
bool SolutionContainer::GRASPAlgorithm(int pos, u64 sum)
{
    int ans[pos];
    int total = 0;
    int distance = 0;
    int i = 0;
    
    while(i < pos && total < sum) {
        int maxi = 0; int poz; bool ok = 0;
        for(int k = 0; k < pos; k++) {
            if(S[k] > maxi && S[k] <= (sum - total) && position[k] == 0) {
                maxi = S[k]; poz = k;
                ok = 1;
            } }
        if(ok) {
            position[poz] = 1; total = total + S[poz]; ans[i] = S[poz];
        }
        i++;}
    distance = sum - total;
    
   // cout << "\nGRASP:\nThe distance from target is: " << distance << endl;
    if(distance != 0)
    {
        int newDistance = GRASP(ans, pos, sum, i, total, distance);
        if(newDistance < distance)
            distance = newDistance;
    }
    if(distance == 0) return true;
    else return false;
}

u64 SolutionContainer::GRASP(int ans[], int pos, u64 sum, int i, int total, int distance)
{
    
    int trys = 0;
    int some1, some2;
    int maximum = sizeof(S)/sizeof(*S); int j;
    
    while(trys < 100 && total < sum) {
        some1= rand() % (maximum - 0 + 1);
        some2 = rand() % (i - 0 + 1);
        j=0;
        for(int k = 0; k<i; k++) if(S[some1] != ans[k]) j++; if(j == i && total-ans[some2]+S[some1] < sum) {
            total = total - ans[some2] + S[some1]; ans[some2] = S[some1];
            distance = sum - total;
        }
        trys++; }
    
    //return distance;
    
      return distanceFromTarget();
}






